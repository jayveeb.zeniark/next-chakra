import { useState } from "react"
import type { NextPage } from "next"
import Head from "next/head"
import NextLink from "next/link"
import { useRouter } from "next/router"

import { 
  Box, 
  Link, 
  Container, 
  Flex, 
  Image, 
  FormControl, 
  Button, 
  FormLabel, 
  Spacer, 
  Heading, 
  VStack, 
  Stack,
  Spinner } from "@chakra-ui/react"

import { BasicInput, PasswordInput, BasicModal } from "../src/components"
import { hasEmptyProperty } from "../src/utils/helper"

import LogoIcon from "../src/assets/img/zeniark-logo.png"
import styles from "../src/styles/modules/SignUp.module.scss"

const SignUp: NextPage = () => {
  const router = useRouter()

  const [values, setValues] = useState({
    name: '',
    email: '',
    contactNo: '',
    username: '',
    password: ''
  })
  const [signupLoading, setSignupLoading] = useState(false)
  const [signupValid, setSignupValid] = useState(false)
  const [resendEmail, setResendEmail] = useState(false)
  const [resendModalOpen, setResendModalOpen] = useState(false)

  const handleChange = (name: string) => (event: any) => {
    setValues({...values, [name]: event.target.value})
  }

  const handleSignUp = () => {
    const isEmpty = hasEmptyProperty(values)

    if (!isEmpty) {
      setSignupLoading(true)

      setTimeout(() => {
        setSignupValid(true)
      }, 1000)
    }
  }

  const handleResendEmail = () => {
    setResendEmail(true)

    setTimeout(() => {
      setResendEmail(false)
      setResendModalOpen(true)
    }, 2000)
  }

  const handleLogin = () => {
    router.push('/')
  }

  const handleResendModalClose = () => {
    setResendModalOpen(false)
  }

  return (
    <Container maxW="container.xl">
      <Head>
        <title>Sign Up</title>
      </Head>

      <Flex justify="space-between" alignItems="center" className={styles.signupHeader}>
        <Image src={LogoIcon.src} alt="Logo" className={styles.signupLogo} />
        <NextLink href="login">
          <Link>Sign in</Link>
        </NextLink>       
      </Flex>

      {signupValid ?
        (
          <Box className={styles.verifyBox}>
            <VStack justify="center">
              <Heading size="md" className={styles.verifyHeading}>Please verify your email</Heading>
              <Box className={styles.verifyContent}>
                <Stack spacing={6}>
                  <Box>
                    You’re almost there! We have sent an email to {values.email}
                  </Box>
                  <Box>
                    Just click on the link in that email to complete your singup. If you don’t see it, you may need to check your spam folder.
                  </Box>
                </Stack>
              </Box>
              <Box className={styles.verifyBtnGroup}>
                {resendEmail ?
                  <Box className={styles.verifyResendEmail}><Spinner size="sm" /></Box> :
                  <Box onClick={handleResendEmail} className={styles.verifyResendEmail}>Resend verification Email</Box>
                }
                <Button onClick={handleLogin}>Click to Login</Button>
              </Box>
            </VStack>

            <BasicModal isOpen={resendModalOpen} onClose={handleResendModalClose}>
              <Box className={styles.verifyModal}>
                <VStack spacing={8}>
                  <Heading size="md">Email Verification Sent</Heading>
                  <Box>
                    We have sent an email to {values.email}
                  </Box>
                  <Button onClick={handleResendModalClose}>Confirm</Button>
                </VStack>
              </Box>
            </BasicModal>
          </Box>
        ) : (
          <Box className={styles.signupForm}>
            <Heading as="h1" className={styles.signupHeading}>Sign Up Freedom Corner</Heading>
            <form>
              <Flex justify="space-between" flexWrap="wrap" className={styles.signupFormWrapper}>
                <Box flex="1">
                  <FormControl>
                    <FormLabel htmlFor="name">Name</FormLabel>
                    <BasicInput 
                      id="email"
                      value={values.name}
                      setValue={handleChange('name')}
                    />
                  </FormControl>
                  <FormControl>
                    <FormLabel htmlFor="name">Email Address</FormLabel>
                    <BasicInput 
                      id="email"
                      value={values.email}
                      setValue={handleChange('email')}
                    />
                  </FormControl>
                  <FormControl>
                    <FormLabel htmlFor="name">Contact Number</FormLabel>
                    <BasicInput 
                      id="contactNo"
                      value={values.contactNo}
                      setValue={handleChange('contactNo')}
                    />
                  </FormControl>
                </Box>
                <Spacer flex=".1" />
                <Box flex="1">
                  <FormControl>
                    <FormLabel htmlFor="name">Username</FormLabel>
                    <BasicInput 
                      id="username"
                      value={values.username}
                      setValue={handleChange('username')}
                    />
                  </FormControl>
                  <FormControl>
                    <FormLabel htmlFor="name">Password</FormLabel>
                    <PasswordInput value={values.password} setValue={handleChange('password')} />
                  </FormControl>
                </Box>
              </Flex>
              <Box className={styles.signupBtn}>
                <Button onClick={handleSignUp} isLoading={signupLoading} loadingText="Sign Up">
                  Sign Up
                </Button>
              </Box>
            </form>
          </Box>
        )
      }
    </Container>
  )
}

export default SignUp
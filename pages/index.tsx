import { useState } from "react"
import { useRouter } from "next/router"
import type { NextPage } from 'next'

import { Formik, Form, FormikHelpers, Field } from "formik"
import { InferType } from "yup"
import { 
  Flex,
  Box,
  Button, 
  Stack,
  Heading,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
  useToast,
  FormControl,
  FormLabel,
  Center,
  FormErrorMessage,} from "@chakra-ui/react"
import { FaSearch } from "react-icons/fa"

import { Layout, BasicInput, BasicModal } from "../src/components"
import { clients } from "../src/data/clients"
import { clientFormSchema } from "../src/utils/validation/schema"
import { getRole } from "../src/utils/auth"

type FormValues = InferType<typeof clientFormSchema>

const Client: NextPage = () => {
  const router = useRouter()
  const toast = useToast()
  const role = getRole()

  const [isFormOpen, setFormOpen] = useState(false)
  const [formType, setFormType] = useState('')
  const [isRemoveOpen, setRemoveOpen] = useState(false)
  const [loadingRemove, setLoadingRemove] = useState(false)

  const initialValues = {
    client: ''
  }

  const handleClientFormModal = (type: string) => {
    setFormType(type)
    setFormOpen(!isFormOpen)
  }

  // Client Form
  const handleClientForm = (values: Object, actions: any) => {
    console.log(JSON.stringify(values, null, 2), 'ADD CLIENT VALUES')

    setTimeout(() => {
      if (formType === 'add') {
        router.push('/client/domain')
      } else {
        toast({
          title: `Client Updated!`,
          status: 'success',
          duration: 3000,
          position: 'top',
          variant: 'subtle'
        })
      }
      
      actions.setSubmitting(false)
      setFormOpen(false)
    }, 1000)
  }

  const handleViewClient = () => {
    router.push('/client/domain')
  }

  // Remove Client
  const handleRemoveClient = () => {
    setRemoveOpen(!isRemoveOpen)
  }

  const handleConfirmRemove = () => {
    setLoadingRemove(true)

    setTimeout(() => {
      setLoadingRemove(false)
      setRemoveOpen(false)

      toast({
        title: 'Successfully removed client!',
        status: 'success',
        duration: 3000,
        position: 'top',
        variant: 'subtle'
      })
    }, 1000)
  }

  return (
    <Layout head="Clients">
      <Box>
        <Flex justify="space-between" align="center">
          <Heading as="h1">Clients</Heading>
          <Stack spacing={4} direction="row" align="center">
            <Box>
              <BasicInput
                icon={<FaSearch className="fa-icon" />} 
                placeholder="Search"
                white
                noMargin
              />
            </Box>
            {role === 'ADMIN' && <Button onClick={() => handleClientFormModal('add')}>ADD NEW</Button>}
          </Stack>
        </Flex>
        
        <Box p={7}>
          <TableContainer>
            <Table variant="unstyled">
              <Thead>
                <Tr>
                  <Th>Client</Th>
                  <Th>Actions</Th>
                </Tr>
              </Thead>
              <Tbody>
                {clients.map(client => (
                  <Tr key={client.id}>
                    <Td>{client.name}</Td>
                    <Td>
                      <Stack spacing={4} direction="row" align="center">
                        <Button onClick={handleViewClient}>VIEW</Button>
                        {role === 'ADMIN' && (
                          <>
                            <Button className="btn-green" onClick={() => handleClientFormModal('edit')}>EDIT</Button>
                            <Button className="btn-red" onClick={handleRemoveClient}>REMOVE</Button>
                          </>
                        )}
                      </Stack>
                    </Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
        </Box>

        {/* Add / Edit Client Modal */}
        <BasicModal isOpen={isFormOpen} onClose={() => handleClientFormModal('')} closeBtn>
          <Box>
            <Center mb={4}>
              <Heading size="md">
                {formType === 'add' ? 'Add New Client' : 'Edit Client'}
              </Heading>
            </Center>
            <Formik
              initialValues={initialValues}
              validationSchema={clientFormSchema}
              onSubmit={(values: FormValues, actions: FormikHelpers<FormValues>) => {
                handleClientForm(values, actions)
              }}
            >
              {(props) => (
                <Form>
                  <Field name="client">
                    {({ field, form }: any) => (
                      <FormControl isInvalid={form.errors.client && form.touched.client}>
                        <FormLabel htmlFor="client">Client</FormLabel>
                        <BasicInput
                          id="client"
                          field={field}
                          disabled={props.isSubmitting}
                        />
                        <FormErrorMessage>{form.errors.client}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Button
                    isFullWidth
                    mt={3}
                    type="submit"
                    isLoading={props.isSubmitting}
                    loadingText={formType === 'add' ? 'Proceed' : 'Submit'}
                  >
                    {formType === 'add' ? 'Proceed' : 'Submit'}
                  </Button>
                </Form>
              )}
            </Formik>
          </Box>
        </BasicModal>

        {/* Remove Client Modal */}
        <BasicModal isOpen={isRemoveOpen} onClose={handleRemoveClient}>
          <Box>
            <Heading size="md">LeadGen</Heading>
            <Box pt={8} pb={14}>Are you sure you want to remove this client?</Box>
            <Stack spacing={3} direction="row" justify="flex-end" align="center">
              <Button 
                onClick={handleConfirmRemove} 
                isLoading={loadingRemove} 
                loadingText="YES" 
                className="btn-red"
              >
                YES
              </Button>
              <Button onClick={handleRemoveClient} disabled={loadingRemove}>CANCEL</Button>
            </Stack>
          </Box>
        </BasicModal>
      </Box>
    </Layout>
  )
}

export default Client
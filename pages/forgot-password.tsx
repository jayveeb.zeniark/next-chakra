import { useState } from "react"
import type { NextPage } from 'next'
import Head from 'next/head'
import NextLink from "next/link"

import { Formik, Form, FormikHelpers, Field } from "formik"
import { InferType } from "yup"
import { Box, Image, FormControl, Button, Link, Center, Heading, FormErrorMessage } from "@chakra-ui/react"
import { FaEnvelope } from "react-icons/fa"

import { BasicInput } from "../src/components"
import { forgotPassFormSchema } from "../src/utils/validation/schema"

import LogoIcon from "../src/assets/img/zeniark-logo.png"
import styles from '../src/styles/modules/Authenticate.module.scss'

type FormValues = InferType<typeof forgotPassFormSchema>

const ForgotPassword: NextPage = () => {
  const [requestSent, setRequestSent] = useState(false)

  const initialValues = {
    email: ''
  }

  const handleRequestReset = (values: Object, actions: any) => {
    console.log(JSON.stringify(values, null, 2), 'FORGOT PASS VALUES')
    
    setTimeout(() => {
      actions.setSubmitting(false)
      setRequestSent(true)
    }, 1000)
  }

  return (
    <Box className={styles.authContainer}>
      <Head>
        <title>Forgot Password</title>
      </Head>

      <Box className={styles.authForm}>
        <Image src={LogoIcon.src} alt="logo" className={styles.authFormLogo} />
        <Heading size="md" className={styles.authHeading}>Forgot Password</Heading>

        {requestSent ?
          <Box className={styles.authFormInfo}>Please check your email for instructions to reset your password</Box>
          :
          <>
            <Box className={styles.authFormInfo}>Please enter your email address to reset your password</Box>

            <Formik
              initialValues={initialValues}
              validationSchema={forgotPassFormSchema}
              onSubmit={(values: FormValues, actions: FormikHelpers<FormValues>) => {
                handleRequestReset(values, actions)
              }}
            >
              {(props) => (
                <Form>
                  <Field name="email">
                    {({ field, form }: any) => (
                      <FormControl isInvalid={form.errors.email && form.touched.email}>
                        <BasicInput
                          id="email"
                          field={field}
                          icon={<FaEnvelope className="fa-icon" />}
                          placeholder="Email"
                          disabled={props.isSubmitting}
                        />
                        <FormErrorMessage>{form.errors.email}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Button
                    type="submit"
                    isLoading={props.isSubmitting} 
                    loadingText="Request reset password"
                    isFullWidth
                    my={3}
                  >
                    Request reset password
                  </Button>
                </Form>
              )}
            </Formik>
          </>
        }

        <Center>
          <NextLink href="/login">
            <Link>Back to Login?</Link>
          </NextLink>
        </Center>
      </Box>
    </Box>
  )
}

export default ForgotPassword

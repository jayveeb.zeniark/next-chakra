import type { NextPage } from 'next'
import Head from 'next/head'
import NextLink from "next/link"
import { useRouter } from "next/router"

import { Formik, Form, Field, FormikHelpers  } from "formik"
import { InferType } from "yup"
import { Box, Image, FormControl, Button, Link, Flex, Heading, Checkbox, FormErrorMessage } from "@chakra-ui/react"
import { LockIcon } from "@chakra-ui/icons"
import { FaUser } from "react-icons/fa"

import { BasicInput, PasswordInput } from "../src/components"
import { authenticateUser } from "../src/utils/auth"
import { loginFormSchema } from "../src/utils/validation/schema"

import LogoIcon from "../src/assets/img/zeniark-logo.png"
import styles from '../src/styles/modules/Authenticate.module.scss'

type FormValues = InferType<typeof loginFormSchema>

const Login: NextPage = () => {
  const router = useRouter()

  const initialValues = {
    username: '',
    password: '',
    remeber: false
  }

  const handleLogin = (values: Object, username: string, actions: any) => {
    console.log(JSON.stringify(values, null, 2), 'LOGIN VALUES')

    setTimeout(() => {
      let role = 'USER'

      if (username === 'admin') {
        role ='ADMIN'
      }

      authenticateUser('authenticated', role)
      actions.setSubmitting(false)
      router.push('/')
    }, 1000)
  }
  
  return (
    <Box className={styles.authContainer}>
      <Head>
        <title>Login</title>
      </Head>

      <Box className={styles.authForm}>
        <Image src={LogoIcon.src} alt="logo" className={styles.authFormLogo} />
        <Heading size="md" className={styles.authHeading}>Log in to your account</Heading>

        <Formik
          initialValues={initialValues}
          validationSchema={loginFormSchema}
          onSubmit={(values: FormValues, actions: FormikHelpers<FormValues>) => {
            handleLogin(values, values.username, actions)
          }}
        >
          {(props) => (
            <Form>
              <Field name="username">
                {({ field, form }: any) => (
                  <FormControl isInvalid={form.errors.username && form.touched.username}>
                    <BasicInput 
                      id="username"
                      field={field}
                      icon={<FaUser className="fa-icon" />} 
                      placeholder="Username"
                      disabled={props.isSubmitting}
                    />
                    <FormErrorMessage>{form.errors.username}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>

              <Field name="password">
                {({ field, form }: any) => (
                  <FormControl isInvalid={form.errors.password && form.touched.password}>
                    <PasswordInput 
                      field={field}
                      icon={<LockIcon />}
                      placeholder="Password"
                      disabled={props.isSubmitting}
                    />
                    <FormErrorMessage>{form.errors.password}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>

              {/* <Flex justify="space-between" align="center" marginY={6}>
                <Field name="remeber">
                  {({ field }: any) => (
                    <Checkbox {...field} id="remember" disabled={props.isSubmitting}>Remember me</Checkbox>
                  )}
                </Field>
                <NextLink href="/forgot-password">
                  <Link>Forgot Password?</Link>
                </NextLink>
              </Flex> */}

              <Button
                type="submit"
                isLoading={props.isSubmitting}
                loadingText="Login"
                isFullWidth
                mt={3}
              >
                Login
              </Button>
            </Form>
          )}
        </Formik>
      </Box>
    </Box>
  )
}

export default Login

import type { NextPage } from "next"
import { useRouter } from "next/router"

import { 
  Box, 
  Flex, 
  Heading, 
  Button, 
  Select,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer, } from "@chakra-ui/react"
import { FaSearch } from "react-icons/fa"

import { Layout, BasicInput } from "../src/components"

import { serverActivity } from "../src/data/server-activity"

const ServerActivity: NextPage = () => {
  const router = useRouter()

  return (
    <Layout head="Server Activity">
      <Box>
        <Flex justify="space-between" align="center">
          <Heading as="h1">Server Activity</Heading>
          <Button onClick={() => router.back()}>GO BACK</Button>
        </Flex>
        <Flex mt={8} justify="space-between" align="center">
          <Box>
            <Select placeholder="Select Server" className="white-select">
              <option value='option1'>ZeniarkBms</option>
              <option value='option2'>AWS Sigmabird Server</option>
              <option value='option3'>ZeniarkMT</option>
              <option value='option4'>Zeniarksms</option>
            </Select>
          </Box>
          <Box>
            <BasicInput 
              icon={<FaSearch className="fa-icon" />} 
              placeholder="Search"
              white
              noMargin
            />
          </Box>
        </Flex>

        <Box p={7}>
          <TableContainer>
            <Table variant="unstyled">
              <Thead>
                <Tr>
                  <Th>NAME</Th>
                  <Th>USER</Th>
                  <Th>ACTIVITY</Th>
                  <Th>DATE</Th>
                </Tr>
              </Thead>
              <Tbody>
                {serverActivity.map(log => (
                  <Tr key={log.id}>
                    <Td>{log.name}</Td>
                    <Td>{log.user}</Td>
                    <Td>{log.activity}</Td>
                    <Td>{log.date}</Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
        </Box>
      </Box>
    </Layout>
  )
}

export default ServerActivity
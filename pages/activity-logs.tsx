import type { NextPage } from "next"
import { useRouter } from "next/router"
import { FaSearch } from "react-icons/fa"

import { 
  Box,
  Flex,
  Heading,
  Stack,
  Button,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer, } from "@chakra-ui/react"

import { Layout, BasicInput } from "../src/components"
import { activityLogs } from "../src/data/activity-logs"

const ActivityLogs: NextPage = () => {
  const router = useRouter()

  return (
    <Layout head="Activity Logs">
      <Box>
        <Flex justify="space-between" align="center">
          <Heading as="h1">Activity Logs</Heading>
          <Stack spacing={4} direction="row" align="center">
            <Box>
              <BasicInput 
                icon={<FaSearch className="fa-icon" />} 
                placeholder="Search"
                white
                noMargin
              />
            </Box>
            <Button onClick={() => router.back()}>GO BACK</Button>
          </Stack>
        </Flex>

        <Box p={7}>
          <TableContainer>
            <Table variant="unstyled">
              <Thead>
                <Tr>
                  <Th>USER</Th>
                  <Th>ACTIVITY</Th>
                  <Th>DATE</Th>
                </Tr>
              </Thead>
              <Tbody>
                {activityLogs.map(log => (
                  <Tr key={log.id}>
                    <Td>{log.user}</Td>
                    <Td>{log.activity}</Td>
                    <Td>{log.datetime}</Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
        </Box>
      </Box>
    </Layout>
  )
}

export default ActivityLogs
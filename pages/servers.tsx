import { useState } from "react"
import type { NextPage } from "next"
import { Formik, Form, FormikHelpers, Field } from "formik"
import { InferType } from "yup"
import { useRouter } from "next/router"
// import { FaSearch } from "react-icons/fa"

import { 
  Box, 
  Flex, 
  Heading, 
  Stack, 
  Button,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
  useToast,
  Center,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Textarea } from "@chakra-ui/react"

import { Layout, BasicModal, BasicInput } from "../src/components"
import { serverFormSchema } from "../src/utils/validation/schema"
import { servers } from "../src/data/servers"

type FormValues = InferType<typeof serverFormSchema>

const Servers: NextPage = () => {
  const router = useRouter()
  const toast = useToast()

  const [isFormOpen, setFormOpen] = useState(false)
  const [formType, setFormType] = useState('')
  const [isRemoveOpen, setRemoveOpen] = useState(false)
  const [loadingRemove, setLoadingRemove] = useState(false)

  const initialValues = {
    name: '',
    ip: '',
    description: ''
  }

  // View server activity
  const handleServerActivity = () => {
    router.push('/server-activity')
  }

  // Server form modal
  const handleServerFormModal = (type: string) => {
    setFormType(type)
    setFormOpen(!isFormOpen)
  }

  const handleServerForm = (values: Object, actions: any) => {
    console.log(JSON.stringify(values, null, 2), 'ADD SERVER VALUES')

    setTimeout(() => {
      toast({
        title: formType === 'add' ? `Server Added!` : `Server Updated!`,
        status: 'success',
        duration: 3000,
        position: 'top',
        variant: 'subtle'
      })
      
      actions.setSubmitting(false)
      setFormOpen(false)
    }, 1000)
  }
  
  // Remove Server
  const handleRemoveServer = () => {
    setRemoveOpen(!isRemoveOpen)
  }

  const handleConfirmRemove = () => {
    setLoadingRemove(true)

    setTimeout(() => {
      setLoadingRemove(false)
      setRemoveOpen(false)

      toast({
        title: 'Successfully removed server!',
        status: 'success',
        duration: 3000,
        position: 'top',
        variant: 'subtle'
      })
    }, 1000)
  }

  return (
    <Layout head="Servers">
      <Box>
        <Flex justify="space-between" align="center">
          <Heading as="h1">Servers</Heading>
          <Stack spacing={4} direction="row" align="center">
            {/* <Box>
              <BasicInput
                icon={<FaSearch className="fa-icon" />} 
                placeholder="Search"
                white
                noMargin
              />
            </Box> */}
            <Button onClick={() => handleServerFormModal('add')}>ADD NEW</Button>
            <Button onClick={handleServerActivity}>VIEW ACTIVITY</Button>
          </Stack>
        </Flex>

        <Box p={7}>
          <TableContainer>
            <Table variant="unstyled">
              <Thead>
                <Tr>
                  <Th>Name</Th>
                  <Th>IP Address</Th>
                  <Th>Last Updated</Th>
                  <Th>Actions</Th>
                </Tr>
              </Thead>
              <Tbody>
                {servers.map(server => (
                  <Tr key={server.id}>
                    <Td>{server.name}</Td>
                    <Td>{server.ip}</Td>
                    <Td>{server.lastUpdate}</Td>
                    <Td>
                      <Stack spacing={4} direction="row" align="center">
                        <Button className="btn-green" onClick={() => handleServerFormModal('edit')}>EDIT</Button>
                        <Button className="btn-red" onClick={handleRemoveServer}>REMOVE</Button>
                      </Stack>
                    </Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
        </Box>

        {/* Add / Edit Server Modal */}
        <BasicModal isOpen={isFormOpen} onClose={() => handleServerFormModal('')} closeBtn>
          <Box>
            <Center mb={4}>
              <Heading size="md">
                {formType === 'add' ? 'Add New Server' : 'Edit Server'}
              </Heading>
            </Center>
            <Formik
              initialValues={initialValues}
              validationSchema={serverFormSchema}
              onSubmit={(values: FormValues, actions: FormikHelpers<FormValues>) => {
                handleServerForm(values, actions)
              }}
            >
              {(props) => (
                <Form>
                  <Field name="name">
                    {({ field, form }: any) => (
                      <FormControl isInvalid={form.errors.name && form.touched.name}>
                        <FormLabel htmlFor="name">Name</FormLabel>
                        <BasicInput
                          id="name"
                          field={field}
                          disabled={props.isSubmitting}
                        />
                        <FormErrorMessage>{form.errors.name}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Field name="ip">
                    {({ field, form }: any) => (
                      <FormControl isInvalid={form.errors.ip && form.touched.ip}>
                        <FormLabel htmlFor="ip">IP Address</FormLabel>
                        <BasicInput
                          id="ip"
                          field={field}
                          disabled={props.isSubmitting}
                        />
                        <FormErrorMessage>{form.errors.ip}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Field name="description">
                    {({ field, form }: any) => (
                      <FormControl isInvalid={form.errors.description && form.touched.description}>
                        <FormLabel htmlFor="description">Description</FormLabel>
                        <Textarea
                          id="description"
                          {...field}
                          disabled={props.isSubmitting}
                        />
                        <FormErrorMessage>{form.errors.description}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Button
                    isFullWidth
                    mt={3}
                    type="submit"
                    isLoading={props.isSubmitting}
                    loadingText="Submit"
                  >
                    Submit
                  </Button>
                </Form>
              )}
            </Formik>
          </Box>
        </BasicModal>

        {/* Remove Server Modal */}
        <BasicModal isOpen={isRemoveOpen} onClose={handleRemoveServer}>
          <Box>
            <Heading size="md">ZeniarkBms</Heading>
            <Box pt={8} pb={14}>Are you sure you want to remove this server?</Box>
            <Stack spacing={3} direction="row" justify="flex-end" align="center">
              <Button 
                onClick={handleConfirmRemove} 
                isLoading={loadingRemove} 
                loadingText="YES" 
                className="btn-red"
              >
                YES
              </Button>
              <Button onClick={handleRemoveServer} disabled={loadingRemove}>CANCEL</Button>
            </Stack>
          </Box>
        </BasicModal>
      </Box>
    </Layout>
  )
}

export default Servers
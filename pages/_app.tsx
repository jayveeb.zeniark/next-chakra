import type { AppProps } from "next/app"
import { ChakraProvider } from "@chakra-ui/react"

import { RouteShield } from "../src/components"

import "../src/styles/global.scss"

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <ChakraProvider>
        <RouteShield>
          <Component {...pageProps} />
        </RouteShield>
      </ChakraProvider>
    </>
  )
}

export default MyApp

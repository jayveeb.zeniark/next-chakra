import { useState } from "react"
import { useRouter } from "next/router"
import type { NextPage } from 'next'

import { Formik, Form, FormikHelpers, Field } from "formik"
import { InferType } from "yup"
import { 
  Flex,
  Box,
  Button, 
  Stack,
  Heading,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
  useToast,
  FormControl,
  FormLabel,
  Center,
  FormErrorMessage,
  Textarea } from "@chakra-ui/react"
import { FaSearch } from "react-icons/fa"

import { Layout, BasicInput, ButtonLink, BasicModal } from "../../src/components"
import { clients } from "../../src/data/clients"
import { domainFormSchema, domainViewFormSchema } from "../../src/utils/validation/schema"
import { getRole } from "../../src/utils/auth"

type FormValues = InferType<typeof domainFormSchema>
type ViewFormValues = InferType<typeof domainViewFormSchema>

const Domain: NextPage = () => {
  const router = useRouter()
  const toast = useToast()
  const role = getRole()

  const [client, setClient] = useState(clients[0])
  const [isFormOpen, setFormOpen] = useState(false)
  const [formType, setFormType] = useState('')
  const [isRemoveOpen, setRemoveOpen] = useState(false)
  const [loadingRemove, setLoadingRemove] = useState(false)
  const [isViewOpen, setViewOpen] = useState(false)

  const initialValues = {
    domain: ''
  }

  const viewInitialValues = {
    purpose: ''
  }

  // Domain Form
  const handleDomainFormModal = (type: string) => {
    setFormType(type)
    setFormOpen(!isFormOpen)
  }

  const handleDomainForm = (values: Object, actions: any) => {  
    console.log(JSON.stringify(values, null, 2), 'DOMAIN VALUES')

    setTimeout(() => {
      if (formType === 'add') {
        router.push('/client/domain-form/add')
      } else {
        toast({
          title: 'Domain Updated!',
          status: 'success',
          duration: 3000,
          position: 'top',
          variant: 'subtle'
        })
      }

      actions.setSubmitting(false)
      setFormOpen(false)
    }, 1000)
  }

  // View Domain Admin
  const handleViewDomain = () => {
    router.push('/client/domain-details')
  }

  // View Domain User
  const handleViewDomainPurpose = () => {
    setViewOpen(!isViewOpen)
  }

  const handleViewDomainForm = (values: Object, actions: any) => {  
    console.log(JSON.stringify(values, null, 2), 'VIEW DOMAIN VALUES')

    setTimeout(() => {
      router.push('/client/domain-details')

      actions.setSubmitting(false)
      setViewOpen(false)
    }, 1000)
  }

  // Remove Domain
  const handleRemoveDomain = () => {
    setRemoveOpen(!isRemoveOpen)
  }

  const handleConfirmRemove = () => {
    setLoadingRemove(true)

    setTimeout(() => {
      setLoadingRemove(false)
      setRemoveOpen(false)

      toast({
        title: 'Successfully removed domain!',
        status: 'success',
        duration: 3000,
        position: 'top',
        variant: 'subtle'
      })
    }, 1000)
  }

  return (
    <Layout head="Domains">
      <Box>
        <Flex justify="space-between" align="center">
          <Heading as="h1">Domains / Subdomains</Heading>
          <Stack spacing={4} direction="row" align="center">
            <Box>
              <BasicInput 
                icon={<FaSearch className="fa-icon" />} 
                placeholder="Search"
                white
                noMargin
              />
            </Box>
            {role === 'ADMIN' && <Button onClick={() => handleDomainFormModal('add')}>ADD NEW</Button>}            
            <ButtonLink link="/" linkText="GO BACK" />
          </Stack>
        </Flex>
        
        <Box p={7}>
          <Heading size="lg" mb={4}>LeadGen</Heading>
          <TableContainer>
            <Table variant="unstyled">
              <Thead>
                <Tr>
                  <Th>Domain / Subdomain</Th>
                  <Th>Actions</Th>
                </Tr>
              </Thead>
              <Tbody>
                {!!client.domain && client.domain.map(item => (
                  <Tr key={item.id}>
                    <Td>{item.name}</Td>
                    <Td>
                      <Stack spacing={4} direction="row" align="center">
                        {role === 'ADMIN' ? <Button onClick={handleViewDomain}>VIEW</Button> : <Button onClick={handleViewDomainPurpose}>VIEW</Button>}
                        {role === 'ADMIN' &&
                          <>
                            <Button className="btn-green" onClick={() => handleDomainFormModal('edit')}>EDIT</Button>
                            <Button className="btn-red" onClick={handleRemoveDomain}>REMOVE</Button>
                          </>
                        }
                      </Stack>
                    </Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
        </Box>

        {/* View Domain Modal for User */}
        <BasicModal isOpen={isViewOpen} onClose={handleViewDomainPurpose} closeBtn>
          <Box>
            <Center mb={4}>
              <Heading size="md">
                Please state your purpose for viewing this domain
              </Heading>
            </Center>

            <Formik
              initialValues={viewInitialValues}
              validationSchema={domainViewFormSchema}
              onSubmit={(values: ViewFormValues, actions: FormikHelpers<ViewFormValues>) => {
                handleViewDomainForm(values, actions)
              }}
            >
              {(props) => (
                <Form>
                  <Field name="purpose">
                    {({ field, form }: any) => (
                      <FormControl isInvalid={form.errors.purpose && form.touched.purpose}>
                        <FormLabel htmlFor="purpose">Purpose</FormLabel>                        
                        <Textarea
                          id="purpose"
                          {...field}
                          disabled={props.isSubmitting}
                        />
                        <FormErrorMessage>{form.errors.purpose}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Button
                    isFullWidth
                    type="submit"
                    isLoading={props.isSubmitting}
                    loadingText="Proceed"
                  >
                    Proceed
                  </Button>
                </Form>
              )}
            </Formik>
          </Box>
        </BasicModal>


        {/* Add/ Edit Domain Modal */}
        <BasicModal isOpen={isFormOpen} onClose={() => handleDomainFormModal('')} closeBtn>
          <Box>
            <Center mb={4}>
              <Heading size="md">
                {formType === 'add' ? 'Add New Domain/Subdomain' : 'Edit Domain/Subdomain'}
              </Heading>
            </Center>

            <Formik
              initialValues={initialValues}
              validationSchema={domainFormSchema}
              onSubmit={(values: FormValues, actions: FormikHelpers<FormValues>) => {
                handleDomainForm(values, actions)
              }}
            >
              {(props) => (
                <Form>
                  <Field name="domain">
                    {({ field, form }: any) => (
                      <FormControl isInvalid={form.errors.domain && form.touched.domain}>
                        <FormLabel htmlFor="domain">Domain</FormLabel>
                        <BasicInput
                          id="domain"
                          field={field}                          
                          disabled={props.isSubmitting}
                        />
                        <FormErrorMessage>{form.errors.domain}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Button
                    isFullWidth
                    mt={3}
                    type="submit"
                    isLoading={props.isSubmitting}
                    loadingText={formType === 'add' ? 'Proceed' : 'Submit'}
                  >
                    {formType === 'add' ? 'Proceed' : 'Submit'}
                  </Button>
                </Form>
              )}
            </Formik>
          </Box>
        </BasicModal>

        {/* Remove Domain Modal */}
        <BasicModal isOpen={isRemoveOpen} onClose={handleRemoveDomain}>
          <Box>
            <Heading size="md">test.leadgen.com</Heading>
            <Box pt={8} pb={14}>Are you sure you want to remove this domain?</Box>
            <Stack spacing={3} direction="row" justify="flex-end" align="center">
              <Button 
                onClick={handleConfirmRemove} 
                isLoading={loadingRemove} 
                loadingText="YES" 
                className="btn-red"
              >
                YES
              </Button>
              <Button onClick={handleRemoveDomain} disabled={loadingRemove}>CANCEL</Button>
            </Stack>
          </Box>
        </BasicModal>
      </Box>
    </Layout>
  )
}

export default Domain
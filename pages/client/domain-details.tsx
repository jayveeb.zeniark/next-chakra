import { useState } from "react"
import type { NextPage } from "next"

import { Formik, Form, FormikHelpers, Field } from "formik"
import { InferType } from "yup"
import { useRouter } from "next/router"
import { 
  Box, 
  Flex, 
  Heading, 
  Stack, 
  Button,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
  FormControl,
  FormLabel,
  Textarea,
  useToast, 
  FormErrorMessage} from "@chakra-ui/react"

import { Layout, ButtonLink, BasicModal } from "../../src/components"
import { logFormSchema } from "../../src/utils/validation/schema"
import { getRole } from "../../src/utils/auth"

import styles from "../../src/styles/modules/Client.module.scss"

type FormValues = InferType<typeof logFormSchema>

const DomainDetails: NextPage = () => {
  const router = useRouter()
  const toast = useToast()
  const role = getRole()

  const [isLogOpen, setLogOpen] = useState(false)

  const initialValues = {
    log: ''
  }

  const handleEditDetails = () => {
    router.push('/client/domain-form/edit')
  }

  const handleLogModal = () => {
    setLogOpen(!isLogOpen)
  }

  const handleLogForm = (values: Object, actions: any) => {
    console.log(JSON.stringify(values, null, 2), 'LOG VALUES')

    setTimeout(() => {
      toast({
        title: `Log Added!`,
        status: 'success',
        duration: 3000,
        position: 'top',
        variant: 'subtle'
      })
      
      actions.setSubmitting(false)
      setLogOpen(false)
    }, 1000)
  }

  return (
    <Layout head="Domain Details">
      <Box>
        <Flex justify="space-between" align="center">
          <Heading size="lg">leadgen.com</Heading>
          <Stack spacing={4} direction="row" align="center">
            <ButtonLink link="/client/domain" linkText="GO BACK" />
            {role === 'ADMIN' && <Button onClick={handleEditDetails}>EDIT</Button>}
          </Stack>
        </Flex>

        <Box className={styles.clientDomainDetails}>
          <TableContainer>
            <Table variant="unstyled">
              <Tbody>
                <Tr>
                  <Td fontWeight="bold">Host</Td>
                  <Td>216.70.114.230</Td>
                </Tr>
                <Tr>
                  <Td fontWeight="bold">Username</Td>
                  <Td>leadgen</Td>
                </Tr>
                <Tr>
                  <Td fontWeight="bold">Password</Td>
                  <Td>123!@#$%</Td>
                </Tr>
                <Tr>
                  <Td fontWeight="bold">Notes/Directory</Td>
                  <Td>/httpdocs</Td>
                </Tr>
              </Tbody>
            </Table>
          </TableContainer>
        </Box>

        <Box mt={5}>
          <Flex justify="space-between" align="center">
            <Heading size="md">Logs:</Heading>
            <Button onClick={handleLogModal}>ADD LOGS</Button>
          </Flex>

          <Box mt={4}>
            <TableContainer>
              <Table variant="unstyled">
                <Thead>
                  <Tr>
                    <Th>User</Th>
                    <Th>Note</Th>
                    <Th>Date</Th>
                  </Tr>
                </Thead>
                <Tbody>
                  <Tr>
                    <Td>ChrisL</Td>
                    <Td>Test Notes</Td>
                    <Td>2022-04-24 23:56:21</Td>
                  </Tr>
                </Tbody>
              </Table>
            </TableContainer>
          </Box>
        </Box>

        <BasicModal isOpen={isLogOpen} onClose={handleLogModal} closeBtn>
          <Box>
            <Heading size="md" mb={8}>leadgen.com</Heading>

            <Formik
              initialValues={initialValues}
              validationSchema={logFormSchema}
              onSubmit={(values: FormValues, actions: FormikHelpers<FormValues>) => {
                handleLogForm(values, actions)
              }}
            >
              {(props) => (
                <Form>
                  <Field name="log">
                    {({ field, form }: any) => (
                      <FormControl isInvalid={form.errors.log && form.touched.log}>
                        <FormLabel htmlFor="log">Add Logs</FormLabel>
                        <Textarea id="log" {...field} disabled={props.isSubmitting} />
                        <FormErrorMessage>{form.errors.log}</FormErrorMessage>
                      </FormControl>
                    )}
                  </Field>

                  <Button 
                    mt={3}
                    type="submit" 
                    isFullWidth 
                    isLoading={props.isSubmitting} 
                    loadingText="Submit"
                  >
                    Submit
                  </Button>
                </Form>
              )}
            </Formik>
          </Box>
        </BasicModal>
      </Box>
    </Layout>
  )
}

export default DomainDetails
import { useState, useEffect } from "react"
import type { NextPage } from "next"
import { useRouter } from "next/router"

import { Formik, Form, FormikHelpers, Field } from "formik"
import { InferType } from "yup"
import { Box, Heading, FormControl, Button, FormLabel, useToast, Textarea, Stack, FormErrorMessage, Select } from "@chakra-ui/react"

import { Layout, BasicInput, PasswordInput } from "../../../src/components"
import { domainDetailsFormSchema } from "../../../src/utils/validation/schema"

import styles from "../../../src/styles/modules/Client.module.scss"

type FormValues = InferType<typeof domainDetailsFormSchema>

const DomainForm: NextPage = () => {
  const router = useRouter()
  const toast = useToast()

  const [formType, setFormType] = useState('')
  const [headText, setHeadText] = useState('')

  const initialValues = {
    server: '',
    host: '',
    username: '',
    password: '',
    note: '',
  }

  useEffect(() => {
    if (!router.isReady) return

    const { type } = router.query

    if (type === 'add') {
      setFormType('add')
      setHeadText('Add Domain Details')
    } else {
      setFormType('edit')
      setHeadText('Edit Credentials')
    }
  }, [router.isReady])

  const handleDomainForm = (values: Object, actions: any) => {
    console.log(JSON.stringify(values, null, 2), 'DOMAIN DETAILS VALUES')

    setTimeout(() => {
      if (formType === 'edit') {
        toast({
          title: `Updated domain details!`,
          status: 'success',
          duration: 3000,
          position: 'top',
          variant: 'subtle'
        })
      }

      actions.setSubmitting(false)
      router.push('/client/domain-details')
    }, 1000)
  }

  const handleCancelEdit = () => {
    router.push('/client/domain-details')
  }
  
  return (
    <Layout head={headText}>
      <Box className={styles.clientWrapper}>
        <Heading className={styles.clientDomain}>leadgen.com</Heading>
        <Heading size="md" className={styles.clientHeading}>
          {formType === 'add' ? 'Enter Domain FTP Details' : 'Update Domain FTP Details'}
        </Heading>

        <Formik
          initialValues={initialValues}
          validationSchema={domainDetailsFormSchema}
          onSubmit={(values: FormValues, actions: FormikHelpers<FormValues>) => {
            handleDomainForm(values, actions)
          }}
        >
          {(props) => (
            <Form>
              <Field name="server">
                {({ field, form }: any) => (
                  <FormControl isInvalid={form.errors.server && form.touched.server}>
                    <FormLabel htmlFor="server">Server</FormLabel>
                    <Select id="server" {...field} disabled={props.isSubmitting} placeholder="Select Server">
                      <option value='option1'>Option 1</option>
                      <option value='option2'>Option 2</option>
                      <option value='option3'>Option 3</option>
                    </Select>
                    <FormErrorMessage>{form.errors.server}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>

              <Field name="host">
                {({ field, form }: any) => (
                  <FormControl isInvalid={form.errors.host && form.touched.host}>
                    <FormLabel htmlFor="host">Host</FormLabel>
                    <BasicInput
                      id="host"
                      field={field}
                      disabled={props.isSubmitting}
                    />
                    <FormErrorMessage>{form.errors.host}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>
              
              <Field name="username">
                {({ field, form }: any) => (
                  <FormControl isInvalid={form.errors.username && form.touched.username}>
                    <FormLabel htmlFor="username">Username</FormLabel>
                    <BasicInput
                      id="username"
                      field={field}
                      disabled={props.isSubmitting}
                    />
                    <FormErrorMessage>{form.errors.username}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>

              <Field name="password">
                {({ field, form }: any) => (
                  <FormControl isInvalid={form.errors.password && form.touched.password}>
                    <FormLabel htmlFor="password">Password</FormLabel>
                    <PasswordInput 
                      field={field}
                      disabled={props.isSubmitting}
                    />
                    <FormErrorMessage>{form.errors.password}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>

              <Field name="note">
                {({ field, form }: any) => (
                  <FormControl isInvalid={form.errors.note && form.touched.note}>
                    <FormLabel htmlFor="note">Note/Directory</FormLabel>
                    <Textarea
                      id="note"
                      {...field}
                      disabled={props.isSubmitting}
                    />
                    <FormErrorMessage>{form.errors.note}</FormErrorMessage>
                  </FormControl>
                )}
              </Field>

              <Stack spacing={4} direction="row" align="center">
                <Button 
                  width={formType === 'add' ? '100%' : '50%'} 
                  mt={3}
                  mb={3}
                  type="submit"
                  isLoading={props.isSubmitting}
                  loadingText={formType === 'add' ? 'Submit' : 'Update'}
                >
                  {formType === 'add' ? 'Submit' : 'Update'}
                </Button>\
                {formType === 'edit' && <Button width="50%" onClick={handleCancelEdit}>Cancel</Button>}
              </Stack>
            </Form>
          )}
        </Formik>
      </Box>
    </Layout>
  )
}

export default DomainForm
import { Box, Image, Spinner, Fade } from "@chakra-ui/react"
import LogoIcon from "../assets/img/zeniark-logo.png"

import styles from "../styles/modules/SplashScreen.module.scss"

export const SplashScreen = () => {
  return (    
    <Box className={styles.splashWrapper}>
      <Box className={styles.splashLogoWrapper}>
        <div className={styles.splashWrapper}>
          <Image src={LogoIcon.src} alt="Logo" className={styles.splashLogo} />
          <Spinner
            thickness='2px'
            speed='0.65s'
            emptyColor='gray.200'
            color='blue.500'
            size='sm'
          />
        </div>
      </Box>
    </Box>
  )
}
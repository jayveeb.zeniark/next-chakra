import { ReactChild, ReactChildren } from "react"

import { 
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  ModalCloseButton, } from "@chakra-ui/react"

interface BasicModalProps {
  isOpen: boolean;
  onClose: any;
  children: ReactChild | ReactChildren;
  centered?: boolean;
  closeBtn?: boolean;
  closeOnOverlayClick?: boolean;
  closeOnEsc?: boolean;
}

export const BasicModal = ({ isOpen, onClose, children, centered, closeBtn, closeOnOverlayClick = false, closeOnEsc = false }: BasicModalProps ) => {
  return (
    <Modal 
      isOpen={isOpen} 
      onClose={onClose} 
      isCentered={centered}
      closeOnOverlayClick={closeOnOverlayClick}
      closeOnEsc={closeOnEsc}
    >
      <ModalOverlay />
      <ModalContent>
        {closeBtn && <ModalCloseButton />}
        <ModalBody>
          {children}
        </ModalBody>
      </ModalContent>
    </Modal>
  )
}
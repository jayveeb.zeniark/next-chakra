import { ReactChild, ReactChildren } from "react"
import Head from "next/head"
import { Box } from "@chakra-ui/react"

import { Header } from "./Header"

interface LayoutProps {
  children: ReactChild | ReactChildren;
  head: string;
}

export const Layout = ({ children, head }: LayoutProps) => {
  return (
    <Box>
      <Head>
        <title>{head}</title>
      </Head>
      <Header />

      <Box className="layoutWrapper">
        {children}
      </Box>
    </Box>
  )
}
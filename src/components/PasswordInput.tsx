import { useState } from "react"
import { InputGroup, InputRightElement, InputLeftElement, Input } from "@chakra-ui/react"
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons"

interface PasswordInputProps {
  field?: any;
  icon?: React.ReactNode;
  placeholder?: string;
  disabled?: boolean;
}

export const PasswordInput = ({ field, placeholder, icon, disabled }: PasswordInputProps) => {
  const [show, setShow] = useState(false)

  const handleShow = () => setShow(!show)

  return (
    <InputGroup>
      {icon && <InputLeftElement children={icon} />}
      <Input 
        {...field}
        id="password"
        type={show ? 'text' : 'password'} 
        placeholder={placeholder}
        _placeholder={{ opacity: 0.4, color: "inherit" }}
        disabled={disabled} 
      />
      <InputRightElement>
        {show ? <ViewOffIcon onClick={handleShow} cursor="pointer" /> : <ViewIcon onClick={handleShow} cursor="pointer" />}
      </InputRightElement>
    </InputGroup>
  )
}
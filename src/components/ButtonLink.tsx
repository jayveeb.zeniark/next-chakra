import NextLink from "next/link"

import { Button, Link } from "@chakra-ui/react"

interface ButtonLinkProps {
  link: string;
  linkText: string;
}

export const ButtonLink = ({ link, linkText }: ButtonLinkProps) => {
  return (
    <NextLink href={link}>
      <Button>
        <Link>{linkText}</Link>
      </Button>
    </NextLink>
  )
}
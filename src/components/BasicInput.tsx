import clsx from "clsx"
import { InputGroup, InputLeftElement, Input } from "@chakra-ui/react"

interface BasicInputProps {
  id?: any;
  field?: any;
  icon?: React.ReactNode;
  placeholder?: string;
  white?: boolean;
  disabled?: boolean;
  noMargin?: boolean;
}

export const BasicInput = ({ id, icon, placeholder, white, disabled, field, noMargin }: BasicInputProps) => {
  return (
    <InputGroup className={clsx(noMargin && 'input-no-margin')}>
      {icon && <InputLeftElement children={icon} />}
      <Input
        {...field}
        id={id}
        placeholder={placeholder} 
        _placeholder={{ opacity: 0.4, color: "inherit" }}
        className={clsx(white && 'input-white-bg')}
        autoComplete="off"        
        disabled={disabled}
      />
    </InputGroup>
  )
}
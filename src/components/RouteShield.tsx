import { useEffect } from "react"
import { NextShield } from "next-shield"
import { useRouter } from "next/router"

import { isUserAuthenticated } from "../utils/auth"
import { SplashScreen } from "../layout"

export const RouteShield = ({ children }: any) => {
  const router = useRouter()
  const isAuth = isUserAuthenticated() || false
  const privateRoutes = ['/', '/client/doamin', '/client/doamin-details', '/client/doamin-form', 'servers', 'activity-logs']
  const publicRoutes = ['/login']
  const pathname = router.pathname  
  
  useEffect(() => {
    if (publicRoutes.includes(pathname)) {
      if (isAuth) {
        router.push('/')
      }
    }
  }, [pathname, isAuth])  

  return (
    <NextShield
      isAuth={isAuth}
      isLoading={false}
      router={router}
      privateRoutes={privateRoutes}
      publicRoutes={publicRoutes}
      accessRoute="/"
      loginRoute="/login"
      LoadingComponent={<SplashScreen />}
    >
      {children}
    </NextShield>
  )
}
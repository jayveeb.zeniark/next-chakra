import { ReactChild, ReactChildren } from "react";
import NextLink from "next/link"
import { Link } from "@chakra-ui/react"

interface BasicLinkProps {
  link: string;
  children: ReactChild | ReactChildren
}

export const BasicLink = ({ link, children }: BasicLinkProps) => {
  return (
    <NextLink href={link}>
      <Link>
        {children}
      </Link>
    </NextLink>
  )
}
import { useRouter } from "next/router"

import { Flex, Image, Avatar, Menu, MenuButton, MenuList, MenuItem, Box, Stack } from "@chakra-ui/react"
import { BasicLink } from "./BasicLink"
import { deauthenticateUser } from "../utils/auth"
import { getRole } from "../utils/auth"

import LogoIcon from "../assets/img/zeniark-logo.png"

export const Header = () => {
  const router = useRouter()
  const role = getRole()

  const handleLogout = () => {
    deauthenticateUser()

    router.push('/login')
  }

  return (
    <Flex justify="space-between" alignItems="center" className="headerWrapper">
      <BasicLink link="/">
        <Image src={LogoIcon.src} alt="logo" className="headerLogo" />
      </BasicLink>

      <Box>
        <Stack spacing={12} direction="row" align="center">
          {role === 'ADMIN' && (
            <Stack spacing={10} direction="row" align="center">|
              <BasicLink link="/">
                Clients
              </BasicLink>
              <BasicLink link="/servers">
                Servers
              </BasicLink>
              <BasicLink link="/activity-logs">
                Activity Logs
              </BasicLink>
            </Stack>
          )}
          <Menu>
            <MenuButton>
              <Avatar name="Zeniark Admin" />
            </MenuButton>
            <MenuList>
              <MenuItem onClick={handleLogout}>Logout</MenuItem>
            </MenuList>
          </Menu>
        </Stack>
      </Box>
    </Flex>
  )
}
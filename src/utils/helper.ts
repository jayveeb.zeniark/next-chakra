export const hasEmptyProperty = (obj: any) => {
  const hasEmpty = Object.values(obj).some(prop => prop === null || prop === '')

  return hasEmpty
}
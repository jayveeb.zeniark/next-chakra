import { setLocale, object, string, bool } from "yup"
import locale from "./locale"
import regex from "../regex"
import validationMessage from "./validationMessage";

setLocale(locale)

export const loginFormSchema = object().shape({
  username: string().required().max(regex.max_sm),
  password: string().required().max(regex.max),
  remember: bool()
})

export const forgotPassFormSchema = object().shape({
  email: string().required().max(regex.max_md).email()
})

export const clientFormSchema = object().shape({
  client: string().required().max(regex.max_md)
})

export const domainFormSchema = object().shape({
  domain: string().required().max(regex.max_md)
})

export const domainViewFormSchema = object().shape({
  purpose: string().required().min(regex.min_xs)
})

export const domainDetailsFormSchema = object().shape({
  server: string().required(),
  host: string().required().max(regex.max),
  username: string().required().max(regex.max_sm),
  password: string().required().max(regex.max).matches(regex.password, validationMessage.passwordCriteria),
  note: string().required()
})

export const logFormSchema = object().shape({
  log: string().required().max(regex.max)
})

export const serverFormSchema = object().shape({
  name: string().required().max(regex.max),
  ip: string().required().max(regex.max_md),
  description: string().required()
})
const validationMessage = {
  required: "This field is required",
  invalidCharacter: "Invalid character exist",
  invalidFormat: "Invalid format",
  maxCharacters: "Maximum characters length exceeded",
  emailAddress: "Must be a valid email address",
  privacyPolicy: "Must accept Privacy Policy",
  passwordCriteria: "Must contain 8 characters, one uppercase, one lowercase, one number and one special character",
  invalidZipCode: "Invalid Postal / Zip Code",
  invalidNumber: "Must be a valid number",
};

export default validationMessage
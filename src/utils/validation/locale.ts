import validationMessage from "./validationMessage";

const locale = {
  mixed: {
    default: "Invalid",
    required: validationMessage.required,
    oneOf: "Must be one of the following values: ${values}",
    notOneOf: "Must not be one of the following values: ${values}",
    defined: "Must be defined",
  },
  string: {
    length: "Must be exactly ${length} characters",
    min: "Must be at least ${min} characters",
    max: "Maximum characters length exceeded",
    matches: validationMessage.invalidCharacter,
    email: validationMessage.emailAddress,
    url: "Must be a valid URL",
    trim: "Must be a trimmed string",
    lowercase: "Must be a lowercase string",
    uppercase: "Must be an upper case string",
  },
  number: {
    min: "Must be greater than or equal to ${min}",
    max: "Must be less than or equal to ${max}",
    lessThan: "Must be less than ${less}",
    moreThan: "Must be greater than ${more}",
    notEqual: "Must be not equal to ${notEqual}",
    positive: "Must be a positive number",
    negative: "Must be a negative number",
    integer: "Must be an integer",
  },
  date: {
    min: "Must be later than ${min}",
    max: "Must be at earlier than ${max}",
  },
};

export default locale;
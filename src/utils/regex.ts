const regex = {
  min_xs: 15,
  max_sm: 60,
  max_md: 128,
  max: 256,
  alpha: /^[a-zA-Z]*$/,
  alphanumeric: /^[0-9A-Za-z]*$/,
  numeric: /^[0-9]\d*$/,
  password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
  email: /^[a-zA-Z0-9][\w.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w.-]*[a-zA-Z0-9]\.[a-zA-Z0-9][a-zA-Z0-9.]*[a-zA-Z]$/,
  name: /^([A-Za-z,.&'-]+ )+[A-Za-z,.&'-]+$|^[A-Za-z,.&'-]+$/,
  contact_no: /^([A-Za-z0-9()-]+ )+[A-Za-z0-9()-]+$|^[A-Za-z0-9()-]+$/,
};

export default regex

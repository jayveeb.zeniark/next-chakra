/**
     * Authenticate a user. Save a token string in Local Storage
     *
     */
  export const authenticateUser = (token: string, role: string) => {
    if (typeof window !== 'undefined') {
      sessionStorage.setItem('token', token);
      sessionStorage.setItem('role', role)
    }
  }

  /**
   * Check if a user is authenticated - check if a token is saved in Local Storage
   *
   * @returns {boolean}
   */
  export const isUserAuthenticated = () => {
    if (typeof window !== 'undefined') {
      return sessionStorage.getItem('token') !== null;
    }
  }

  /**
   * Deauthenticate a user. Remove a token from Local Storage.
   *
   */
  export const deauthenticateUser = () => {
    if (typeof window !== 'undefined') {
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('role')
    }
  }

  /**
   * Get a token value.
   *
   * @returns {string}
   */

  export const getToken = () => {
    if (typeof window !== 'undefined') {
      return sessionStorage.getItem('token');
    }
  }

    /**
   * Get a role value.
   *
   * @returns {string}
   */

  export const getRole = () => {
    if (typeof window !== 'undefined') {
      return sessionStorage.getItem('role');
    }
  }
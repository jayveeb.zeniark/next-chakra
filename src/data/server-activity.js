export const serverActivity = [
  {
    id: 1,
    name: 'ZeniarkBms',
    user: 'John Doe',
    activity: 'Server Restarted',
    date: 'May 5, 2022 08:28 AM'
  },
  {
    id: 2,
    name: 'AWS Sigmabird Server',
    user: 'Luffy Taro',
    activity: 'Server Restarted',
    date: 'May 6, 2022 10:53 AM'
  },
  {
    id: 3,
    name: 'ZeniarkMT',
    user: 'Zoro Juro',
    activity: 'Server Restarted',
    date: 'May 7, 2022 09:21 AM'
  },
  {
    id: 4,
    name: 'Zeniarksms',
    user: 'Law Dono',
    activity: 'Server Restarted',
    date: 'May 7, 2022 02:55 PM'
  },
]
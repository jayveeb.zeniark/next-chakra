export const accountsData = [
  {
    client: 'Client 1',
    credentials: [
      {
        id: 1,
        type: 'FTP',
        description: 'N/A',
        environment: 'development',
        host: '192.180.0.1',
        creds: {
          username: 'admin',
          password: 'zenadmin'
        }
      },
      {
        id: 2,
        type: 'Database',
        description: 'db_name',
        environment: 'development',
        host: 'N/A',
        creds: {
          username: 'admin',
          password: 'zenadmin'
        }
      },
      {
        id: 3,
        type: 'Wordpress',
        description: 'N/A',
        environment: 'development',
        host: 'https://www.sampl-e.com/wp-admin',
        creds: {
          username: 'admin',
          password: 'zenadmin'
        }
      },
    ]
  },
  {
    client: 'Client 2',
    credentials: [
      {
        id: 4,
        type: 'User (Admin)',
        description: 'Notes',
        environment: 'production',
        host: 'https://www.sampl-e2.com/login',
        creds: {
          username: 'admin',
          password: 'zenadmin'
        }
      },
    ]
  },
]
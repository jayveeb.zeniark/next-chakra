export const activityLogs = [
  {
    id: 1,
    user: 'ChrisL',
    activity: 'Logged in',
    datetime: 'May 5, 2022 08:20 AM',
  },
  {
    id: 2,
    user: 'ChrisL',
    activity: 'Added client NewGen',
    datetime: 'May 5, 2022 08:50 AM',
  },
  {
    id: 3,
    user: 'ChrisL',
    activity: 'Edited client NewGen to OldGen',
    datetime: 'May 6, 2022 10:53 AM',
  },
  {
    id: 4,
    user: 'ChrisL',
    activity: 'Removed client OldGen',
    datetime: 'May 7, 2022 02:55 PM',
  },
  {
    id: 5,
    user: 'ChrisL',
    activity: 'Added newgen.com domain to NewGen client',
    datetime: 'May 8, 2022 11:50 AM',
  },
  {
    id: 6,
    user: 'ChrisL',
    activity: 'Edited newgen.com domain to oldgen.com from NewGen client',
    datetime: 'May 10, 2022 04:20 PM',
  },
  {
    id: 7,
    user: 'ChrisL',
    activity: 'Removed newgen.com domain from NewGen client',
    datetime: 'May 15, 2022 03:15 PM',
  },
  {
    id: 8,
    user: 'ChrisL',
    activity: 'Viewed newgen.com FTP details for: Updating content and images',
    datetime: 'May 20, 2022 11:11 AM',
  },
  {
    id: 9,
    user: 'ChrisL',
    activity: 'Logged out',
    datetime: 'May 20, 2022 02:00 PM',
  },
]
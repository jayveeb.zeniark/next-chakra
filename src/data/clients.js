export const clients = [
  {
    id: 1,
    name: 'LeadGen',
    domain: [
      {
        id: 1,
        name: 'leadgen.com'
      },
      {
        id: 2,
        name: 'app.leadgen.com'
      },
      {
        id: 3,
        name: 'dev.leadgen.com'
      },
      {
        id: 4,
        name: 'test.leadgen.com'
      },
      {
        id: 5,
        name: 'uat.leadgen.com'
      },
    ]
  },
  {
    id: 2,
    name: 'LearnZoe',
  },
  {
    id: 3,
    name: 'RipePH',
  },
  {
    id: 4,
    name: 'Zeniark',
  },
  {
    id: 5,
    name: 'FreedomCorner',
  },
  {
    id: 6,
    name: 'TheSoapStore',
  },
  {
    id: 7,
    name: 'MergeMedicalCenter',
  },
]